<?php

/* --------------------------------- *\
 
	Austra

\* --------------------------------- */

// get pages
$page_current = ZetaData::get_pages('austra');
$page_contacts = ZetaData::get_pages('contacts');

// template
echo get_html(array(

	// text
	'title'			=> $page_current->title,
	'button_text'	=> $page_current->button_text,
	'button_link'	=> ZetaData::get_lang_url($page_contacts),

	// content
	'tabs'			=> ZetaTemplates::tabs($url),

	// contacts
	'contacts' 		=> ZetaTemplates::block_contacts(),

	// template
	'template'		=> APP_VIEWS . '/austra/austra.html'

));





// -- __austra.php