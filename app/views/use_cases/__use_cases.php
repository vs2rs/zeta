<?php

/* --------------------------------- *\
 
	Use Cases

\* --------------------------------- */

// template
echo get_html(array(

	// content
	'intro'		=> ZetaTemplates::intro_simple('use-cases'),
	'use_cases' => ZetaTemplates::use_cases(),
	'contacts' 	=> ZetaTemplates::block_contacts(),

	// template
	'template'	=> APP_VIEWS . '/use_cases/use-cases.html'

));





// -- __austra.php