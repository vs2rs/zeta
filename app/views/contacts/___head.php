<?php
	
// get the inputs
$inputs = ZetaForm::inputs($lang);

/* --------------------------------- *\
	 
	Send the form

\* --------------------------------- */

$errors = array();
$valid_post_data = array();
$input_post_data = array();

foreach ($inputs as $input) {

	// set the input type
	$input_type = $input['input_type'];
	
	// get the input template and stuff
	$input_data = ZetaForm::$input_type($input);

	// check for error
	if($input_data['error']) {
		$errors[] = $input_data['error'];
	} else {
		$valid_post_data[] = $input['name'];
	}

	// set the update version of input data with values
	// this is used later to create email template
	$input_post_data[] = $input_data['params'];

}

if(ZetaForm::validate_post($valid_post_data, $errors)) {

	// send the contact form
	ZetaForm::send_contact_form(
		$input_post_data,
		array(
			'subject'		=> Config::read('email_subject'),
			'email_to'		=> Config::read('email_to'),
			'email_from'	=> Config::read('email_from'),
			'email_name'	=> Config::read('email_name'),
		)
	);

}




