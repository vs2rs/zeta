<?php

// resutls
$items = array();

$key = 0;
$items[$key] = new stdClass();
$items[$key]->name 	= 'HQ: Riga / LV';
$items[$key]->company = 'Zeta Industry ';
$items[$key]->address = 'Gustava Zemgala Gatve 74A,<br/>Rīga, LV-1039';
$items[$key]->phone 	= '+371 29 166 224';
$items[$key]->email 	= 'info@zetaindustry.eu';

$key++;
$items[$key] = new stdClass();
$items[$key]->name 	= 'Partner Office: Tallinn / EE';
$items[$key]->company = '<a href="https://leandigital.eu/" target="_blank">Lean Digital</a>';
$items[$key]->address = 'Narva mnt. 7B, 5. floor room 505,<br/>10117, Tallinn, Estonia';
$items[$key]->phone 	= false;
$items[$key]->email 	= 'info@zetaindustry.eu';

$key++;
$items[$key] = new stdClass();
$items[$key]->name 	= 'Partner Office: Kaunas / LT';
$items[$key]->company = '<a target="_blank" href="https://www.linkedin.com/in/andrius-trunovas-185133216/">Andrius Trunovas</a>';
$items[$key]->address = 'Eigulių g. 8, Kaunas';
$items[$key]->phone 	= '+370 602 26 962';
$items[$key]->email 	= 'andrius.trunovas@zetaindustry.eu';

$key++;
$items[$key] = new stdClass();
$items[$key]->name 	= 'Partner Office: Copenhagen / DK';
$items[$key]->company = '<a href="https://co-alliance.io/" target="_blank">Co-Alliance</a>';
$items[$key]->address = 'Njalsgade 21G, 3rd Floor,<br/>2300 Copenhagen, Denmark';
$items[$key]->phone 	= false;
$items[$key]->email 	= 'info@zetaindustry.eu';

$key++;
$items[$key] = new stdClass();
$items[$key]->name 	= 'Partner Office: Novijsad / SB';
$items[$key]->company = 'MAK IT SEE';
$items[$key]->address = 'Bulevar oslobođenja 127,<br/>21000 Novi Sad';
$items[$key]->phone 	= '+381 63 239 987';
$items[$key]->email 	= 'info@zetaindustry.eu';





// -- offices.en.php