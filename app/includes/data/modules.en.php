<?php

// resutls
$items = array();

$key = 1; // start form 1 - we have a info block with 0 index
$items[$key] = new stdClass();
$items[$key]->name = 'CRM';
$items[$key]->url = 'crm';
$items[$key]->icon = 'uploads/modules_icons/crm.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/crm.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p class="text-medium">The sales module is a convenient way to manage all of your sales and orders in one place.</p>
		
	<p>Sales managers can track the order fulfillment progress, in the overview pane, where both list and status views are available. Any changes to order or customer information can be made easily. Preventing edits on some fields depending on order status and/or user roles can be set up in whatever configuration you need.</p>

	<p>The sales module allows your sales managers to work in a single common space ensuring a good flow of information, and makes sure everyone has access to the same up to date data. Making your sales process a lot more elastic and capable of adapting to unexpected challenges.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Order review in list and status views</li>
		<li>Automatic order estimates based on pre-configured product configurations</li>
		<li>Automatic generation of order documentation - offers, contracts, instructions, etc.</li>
		<li>Task planning for order execution</li>
		<li>Communication history</li>
		<li>CRM features</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Project Management';
$items[$key]->url = 'project-management';
$items[$key]->icon = 'uploads/modules_icons/project-management.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/project-management.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p>Project management module was developed for manufacturers involved in larger scale construction or delivery projects. Projects can be divided into orders and the planning interface handles document storage and versioning, resource planning, manufacturing batch planning, statuses, and more to make sure everything related to one project is easy to navigate throughout the whole system.</p>

	<p>Project management module can also act as a hub for information sharing among external parties involved in the project cycle - engineers, manufacturers, logistics companies, and assembly teams. In the case of construction material manufacturers, the module can integrate with CAD platforms to upload and interpret engineering information for further planning and status control.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>CAD/BIM information upload and interpretation</li>
		<li>Order, stage, and batch planning</li>
		<li>Project progress status overview</li>
		<li>Documentation management workflows</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Product Configurator';
$items[$key]->url = 'product-configurator';
$items[$key]->icon = 'uploads/modules_icons/product-configurator.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/product-configurator.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p class="text-medium">Streamline manufacturing and accelerate sales with Austra ERP product configurator!</p>
		
	<p>With Austra ERP your sales team will spend less time working on offer and order documentation, and more acquiring new customers.</p>

	<p>Our product configurator creates dynamic product and service templates. The configurator can be adjusted for whatever your particular needs might be. Whether you have one product that can be configured in a hundred different ways or a myriad of products that need their own configurators, Austra ERP can handle it all.</p>

	<p>We support a low-code approach to allow setting up sophisticated configurations. The setup for the product configurator is done via a simple interface, where your employees can edit existing configurators or add and set up new ones with ease. Our software can handle any parameters your manufactured goods and processes might have, and prepare standardized information for other modules like manufacturing and logistics.</p>

	<p>Once everything has been set up, our platform can automatically calculate BOM, production norms, generate order estimates and any other documentation you might need.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Creation of product groups for easier navigation</li>
		<li>Several easily accessible templates</li>
		<li>Simplified and advanced low-code adding of parameters</li>
		<li>BOM setup</li>
		<li>Production time norm setup</li>
		<li>Automatic document generation setup</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Production Planning';
$items[$key]->url = 'production-planning';
$items[$key]->icon = 'uploads/modules_icons/production-planing.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/production-planning.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p class="text-medium">Save time and resources of your manufacturing business with Austra ERP.</p>
		
	<p>Austra ERP is built to suit the particular needs of manufacturing processes, and provides a holistic solution to manage the full lifecycle of a product. Implementing a fully integrated system for manufacturing will enable your employees to make fast and informed decisions when facing unexpected challenges. In response to external or internal factors changes can be made in any step of the manufacturing process, and the relevant employees can instantly be made aware of it, or the system can be set up in a way where changes adjust manufacturing plans automatically.</p>

	<p>With Austra ERP your company will be able to manage sales, estimate prices, oversee manufacturing and optimize logistics in one unified system, thus significantly reducing the time and resources spent on unnecessary meetings, redundant emails and confusing spreadsheets.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Automatic scheduling of production orders</li>
		<li>Manufacturing specification generation and printouts</li>
		<li>Automatic write-offs of materials</li>
		<li>Multi-plant management</li>
		<li>Resource capacity management</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Logistics';
$items[$key]->url = 'logistics';
$items[$key]->icon = 'uploads/modules_icons/logistics.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/logistics.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p class="text-medium">Reduce the work hours your employees spend organizing deliveries.</p>
		
	<p>Well-planned logistics play a huge role in the growth and success of any manufacturing business. Our logistics module enables your business to save time by automating multiple steps of your logistics process.</p>

	<p>Having everything within the same platform allows for a seamless flow from manufacturing to delivery of goods. Once the goods are manufactured our automated packing-list generator can compile your goods into packaging units (pallets, boxes, silos, etc.). Given the parameters of the transport, our system can then efficiently arrange the packaging units into trucks or containers for delivery.</p>

	<p>Once the actual deliveries start going out Austra ERP can generate CMR\'s and any other standard or custom documentation you might need.</p>

	<p>When it comes to logistics, autonomous trucks is the one challenge we\'ll leave to others, let Austra ERP take care of the rest!</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Automatic and manual packing-list planning</li>
		<li>Cargo and transport planning</li>
		<li>Optimization algorithms - space, weight, orientation, loading/unloading sequence</li>
		<li>Preparation of delivery printouts - CMRs, etc.</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Installation Planning';
$items[$key]->url = 'installation-planning';
$items[$key]->icon = 'uploads/modules_icons/installation-planing.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/installation-planing.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p>The module provides an opportunity to plan the installation or assembly of the manufactured products. Assembly and logistics managers can review orders placed for installation at site and schedule manufacturing activities accordingly.</p>

	<p>Since manufacturing and orders are managed in the same place, installation planning schedule can be made based on the expected manufacturing and orders. In case of any deviations from the plan, information is made available instantly to everyone involved in handling the particular order making the planning a lot more elastic and efficient.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Installation planning by elements, products, installation teams</li>
		<li>Work order management</li>
		<li>Status management</li>
		<li>Integration with manufacturing and logistics business unit for real-time status overview</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Warehouse';
$items[$key]->url = 'warehouse';
$items[$key]->icon = 'uploads/modules_icons/warehouse.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/warehouse.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p class="text-medium">Supply-chain management is the oil that keeps a manufacturing business running.</p>

	<p>Avoid any potential downtime due to lacking raw-materials. With Austra ERP you can manage your manufactured goods, and be certain that your manufacturing never runs out of supplies for production.</p>

	<p>The module provides all functions related to warehouse management and execution of procurement orders. Warehouse managers can easily review warehouse balances and arrange new procurement orders as needed. The production module feeds information about estimated remainder at the end of the month, allowing warehouse managers to make sure everything is ordered in a timely manner, according to expected expenditures.  Austra ERP can handle the min-max replenishment process if that\'s how you prefer to organize your stock.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Automatic material write-offs</li>
		<li>Purchase order triggers</li>
		<li>Multi-warehouse support</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Quality Management';
$items[$key]->url = 'quality-management';
$items[$key]->icon = 'uploads/modules_icons/quality-manegement.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/quality-management.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p>Quality management plays an important role in any manufacturing business. We have developed a module for this purpose, where both reporting and review is made easy in order to ensure quality issues are always reported yet don\'t waste too much of your employees time.</p>

	<p>It supports both internal and external claims thus ensuring that all quality issues are managed in one place. Once the information on quality issues has been entered, detailed analysis of the various issues and issue types can be easily accessed and used for further reporting or solution seeking.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Issue reporting - system interface of mobile app</li>
		<li>Issue categorization (process stage and type, business unit, product, etc.) and cause analysis</li>
		<li>Photo, video, documentation features</li>
		<li>Issue resolution workflows and statuses</li>
	</ul>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Tasker';
$items[$key]->url = 'tasker';
$items[$key]->icon = 'uploads/modules_icons/tasker.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/tasker.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p class="text-medium">Every detail matters!</p>

	<p>We\'ve developed Austra ERP to have full product lifecycle support manufacturers, but we are well aware that attention to detail is as important as the big picture. We made the task module to make sure that every task, no matter how big or small, can be handled in a consistent and comfortable way.</p>

	<p>Attaching tasks to specific projects, price offers or sales deals is simple and easy to do. Anyone checking the specific order, or deal can see all pending and completed tasks.</p>

	<p>There are many meetings that could have been emails, with Austra ERP you\'ll be able to go even further and avoid lengthy emails in favour of assigning tasks that have all the necessary context right there.</p>
';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Mobile App';
$items[$key]->url = 'mobile-app';
$items[$key]->icon = 'uploads/modules_icons/mobile-app.svg';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/modules_images/mobile-app.png';
$items[$key]->img->alt = '';
$items[$key]->content = '
	<p class="text-medium">Fast and precise flow of information is a must, when it comes to running your manufacturing business efficiently.</p>

	<p>Our mobile app allows you to have have access to Austra ERP on the go. Make sure your sales team can track orders and make updates instantly, thus ensuring timely and precise information flow. Managers can overview, and get notifications on issues emergencies and other urgent issues that may come up during work.</p>

	<p>When it comes to manufacturing and quality control, having a computer next to you won\'t always be practical or convenient. With Austra ERP mobile app your employees won\'t have to run to a computer everytime they need to do small updates, or compare the physical reality to what\'s shown in the system. With the app making quick status changes comments or other updates via your smartphone is made simple and easy for everyone.</p>
	
	<p>Manufacturing planning and updates depending on the real situation will be done quickly, thus the sales team can be informed and act accordingly as well. So even if there are some issues in any stage of the process, your team will be able to be informed and react accordingly.</p>

	<p><strong>Key features:</strong></p>

	<ul>
		<li>Barcode/QR generation and scanning</li>
		<li>Photo/video recording</li>
		<li>Element status overview</li>
		<li>Time reporting - planned vs actual</li>
	</ul>
';





// -- modules.en.php