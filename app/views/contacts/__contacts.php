<?php

/* --------------------------------- *\
 
	Setup the Form

\* --------------------------------- */

// inputs
$inputs = ZetaForm::inputs($lang);

// set up some params
$errors = array();
$valid_post_data = array();
$input_post_data = array();
$form_html = array();

foreach ($inputs as $input) {

	// set the input type
	$input_type = $input['input_type'];
	$input_name = $input['name'];
	
	// get the input template and stuff
	$input_data = ZetaForm::$input_type($input);

	// check for error
	if($input_data['error']) {
		$errors[$input_name] = $input_data['error'];
	} else {
		$valid_post_data[] = $input['name'];
	}

	// set the update version of input data with values
	// this is used later to create email template
	$input_post_data[] = $input_data['params'];

	// add to html
	$form_html[$input_name] = $input_data['html'];

}





/* --------------------------------- *\
 
	Check for post

\* --------------------------------- */

if(ZetaForm::validate_post($valid_post_data, $errors)) {

	// send the contact form
	ZetaForm::send_contact_form(
		$input_post_data,
		array(
			'subject'		=> Config::read('email_subject'),
			'email_to'		=> Config::read('email_to'),
			'email_from'	=> Config::read('email_from'),
			'email_name'	=> Config::read('email_name'),
		)
	);

}





/* --------------------------------- *\
 
	Contacts

\* --------------------------------- */

// check status
if(isset($_GET['send']) && $_GET['send'] == 'success') {

	$contact_form = get_html(array(

		'form_title'	=> ZetaForm::lang($lang, 'form_success'),

		// template
		'template'		=> APP_VIEWS . '/contacts/contacts-send.html'

	));

} else if(isset($_GET['send']) && $_GET['send'] == 'error') {

	$contact_form = get_html(array(

		'form_title'	=> ZetaForm::lang($lang, 'send_error'),

		// template
		'template'		=> APP_VIEWS . '/contacts/contacts-send.html'

	));

} else {

	// template
	$contact_form = get_html(array(

		// form text and inputs
		'form_title'	=> ZetaForm::lang($lang, 'form_title'),
		'name'			=> $form_html['zeta_name'],
		'email'			=> $form_html['zeta_email'],
		'text'			=> $form_html['zeta_text'],
		'submit'		=> ZetaForm::lang($lang, 'submit_button'),

		// template
		'template'		=> APP_VIEWS . '/contacts/contacts-form.html'

	));

}

// template
echo get_html(array(

	// content
	'intro'		=> ZetaTemplates::intro_simple('contacts'),
	'offices'	=> ZetaTemplates::offices(),
	'form'		=> $contact_form,

	// template
	'template'	=> APP_VIEWS . '/contacts/contacts.html'

));





// -- __contacts.php