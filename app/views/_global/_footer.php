<?php

/* --------------------------------- *\
 
	Menu

\* --------------------------------- */

// html list of our menu items
$footer_nav_html = '';

foreach ($pages as $page_url => $menu_item) {

	// check if show in menu
	if(isset($menu_item->show_in_menu)
	&& $menu_item->show_in_menu != true) {
		continue;
	}

	// active var
	$color = 'main';

	// check if active page
	if($menu_item->template == $page) {
		$color = 'dark';
	}

	// get the html template
	$footer_nav_html .= get_html(array(

		// menu
		'name'		=> $menu_item->name,
		'link'		=> ZetaData::get_lang_url($menu_item),
		'color'		=> $color,

		// template
		'template'	=> APP_VIEWS . '/_global/nav-footer-item.html'

	));

}





/* --------------------------------- *\
 
	Modules

\* --------------------------------- */

// html list of our menu items
$module_nav_html = '';
$austra = ZetaData::get_pages('austra');
$austra_url = ZetaData::get_lang_url($austra);

if($modules = ZetaData::modules()) {

	foreach ($modules as $key => $module_item) {

		// active var
		$color = 'main';

		// check if active page
		if(isset($active_module)
		&& $module_item->url == $active_module) {
			$color = 'dark';
		}

		// get the html template
		$module_nav_html .= get_html(array(

			// menu
			'name'		=> $module_item->name,
			'id'		=> $key + 1,
			'link'		=> $austra_url . '/modules/' . $module_item->url,
			'color'		=> $color,

			// template
			'template'	=> APP_VIEWS . '/_global/footer-module-item.html'

		));

	}

}





/* --------------------------------- *\
 
	Footer

\* --------------------------------- */

// content
$contacts = ZetaData::get_pages('contacts');
$privacy = ZetaData::get_pages('privacy-policy');

// output
echo get_html(array(

	// text
	'title'			=> $contacts->footer_title,
	'follow'		=> $contacts->follow,
	'modules_title'	=> $contacts->modules,
	'makit'			=> $contacts->makit,
	'copyright'		=> $contacts->copyright,
	'year'			=> date('Y'),

	// privacy
	'privacy_text'	=> $privacy->name,
	'privacy_link'	=> ZetaData::get_lang_url($privacy),

	// menus
	'nav'			=> $footer_nav_html,
	'modules'		=> $module_nav_html,

	// template
	'template'	=> APP_VIEWS . '/_global/footer.html'

));





// -- _footer.php