document.addEventListener('DOMContentLoaded', function() {

	// TODO: Need to check correct way to check dom ready with plain js
	// https://www.sitepoint.com/jquery-document-ready-plain-javascript/
	// https://javascript.info/onload-ondomcontentloaded

	/*
	
	<div class="images-load-js">
		<img class="images-set-position-js" />
	</div>

	*/

	/* ------------------------------

		Classes

	------------------------------ */

	// set js classes
	const img_load_js 			= "images-load-js";
	const img_loader_js 		= "images-loader-js";
	const img_loading_js 		= "images-loading-js";
	const img_position_js 		= "images-set-position-js";
	const img_orientation_js 	= "images-set-orientation-js";

	// images classes
	const img_class 			= "image";
	const img_portrait 			= "image__portrait";
	const img_landscape 		= "image__landscape";
	const img_loaded 			= "image__loaded";





	/* ------------------------------

		Loader animatation

	------------------------------ */

	// create the element and add classes
	const loader_animation = document.createElement("div");
	// loader_animation.classList.add(
	// 	"loading", "absolute", "color-gray-dark-bg", "images-loader-js"
	// );

	// // add extra html
	// loader_animation.innerHTML = `<div class="lds-ellipsis"><div class="color-comp-bg"></div><div class="color-comp-bg"></div><div class="color-comp-bg"></div><div class="color-comp-bg"></div></div>`;





	/* ------------------------------

		Setup load event listener

	------------------------------ */

	// get the images
	const loadImages = document.getElementsByClassName(img_load_js);

	// check if there are images
	if(loadImages.length > 0) {
		
		// go through images
		for (let i = 0; i < loadImages.length; i++) {
			
			// get image tags
			const images = loadImages[i].getElementsByTagName("img");

			// check if there are images and setup
			if(images.length > 0) {

				// how many images are in cache
				let images_cache = 0;

				for (let i = 0; i < images.length; i++) {

					// new image
					const image = new Image();

					// check if loaded and do stuff
					image.onload = function() {

						// get coresponding img
						const image = this.element;

						// set loaded class
						image.classList.add(img_loaded);

						// set position if needed
						if(image.classList.contains(img_position_js)) {
							imageSetPosition.call(image);
						}

						// set position if needed
						if(image.classList.contains(img_orientation_js)) {
							imageSetOrientation.call(image);
						}

						// remove loading animation
						imageRemoveLoading.call(image);

					}

					// set src and coresponding element
					image.src = images[i].src;
					image.element = images[i];

					// count cached images
					if(image.complete || image.width + image.height > 0) {
						images_cache++;
					}

					// remove image if error
					image.onerror = function() { images[i].remove(); }

				}

				// add loader if cached count les then total
				if(images_cache < images.length) {
			
					// set loading class
					loadImages[i].classList.add(img_loading_js);

					// add loader animation
					loadImages[i].appendChild(loader_animation.cloneNode(true));

				}

			} else {

				// no images

				// remove loaders
				const loader = loadImages[i].getElementsByClassName(img_loader_js);
				for (let i = 0; i < loader.length; i++) {
					if(loader[i] !== null && loader[i] !== undefined) loader[i].remove();
				}

				// remove loading class
				loadImages[i].classList.remove(img_loading_js);

			}

		}

	}





	/* ------------------------------

		Remove loading animation

	------------------------------ */

	const imageRemoveLoading = function() {

		// set image
		const image = this;

		// get the parent element
		let parent = image.parentElement;
		while(!parent.classList.contains(img_load_js)) {
			parent = parent.parentElement; if(parent === null || parent === undefined) break;
		}

		// remove loader
		const removeLoader = function() {
			const loader = this.getElementsByClassName(img_loader_js);
			for (let i = 0; i < loader.length; i++) {
				if(loader[i] !== null && loader[i] !== undefined) loader[i].remove();
			}
		}

		// if we have parent div
		if(parent && parent !== null && parent !== undefined) {

			// get count of the images
			const images_total = parent.getElementsByTagName("img").length;
			const images_loaded = parent.getElementsByClassName(img_loaded).length;

			// check the count
			if(images_total == images_loaded) {
				
				// remove loading class
				parent.classList.remove(img_loading_js);

				// remove loader
				removeLoader.call(parent);

			}

		}

	}





	/* ------------------------------

		Set position

	------------------------------ */

	const imageSetPosition = function() {

		// set image
		const image = this;

		// get sizes
		const box_width = image.parentElement.offsetWidth;
		const box_height = image.parentElement.offsetHeight;
		const img_width = image.width;
		const img_height = image.height;

		// calculate new sizes
		const new_img_width = box_height * img_width / img_height;
		const new_img_height = box_width * img_height / img_width;

		// function to setup correct class for image
		const changeImageClass = function(set_class, remove_class) {
			// remove class if it's there and clear style
			if(image.classList.contains(remove_class)) {
				image.classList.remove(remove_class);
				image.removeAttribute("style");
			}
			// set the class if it's not set
			if(!image.classList.contains(set_class)) image.classList.add(set_class);
		}

		// check if portraite
		if(new_img_width < box_width) {
			// setup class
			changeImageClass(img_portrait, img_landscape);
			// set new position
			const new_postion = - ( ( new_img_height - box_height ) / 2 );
			// add style
			image.style.top = new_postion + "px";
		}

		// if landscape
		else {
			// setup class
			changeImageClass(img_landscape, img_portrait);
			// set new position
			const new_postion = - ( ( new_img_width - box_width ) / 2 );
			// add style
			image.style.left = new_postion + "px";
		}

		// set align if we have data
		if(image.dataset.align !== null && image.dataset.align !== undefined) {

			// check top or bottom, middle doesn't change
			if(image.dataset.align == "top") {
				image.style.top = 0; image.style.bottom = "auto";
			} else if(image.dataset.align == "bottom") {
				image.style.top = "auto"; image.style.bottom = 0;
			}

		}

	};





	/* ------------------------------

		Set position

	------------------------------ */

	const imageSetOrientation = function() {

		// set image
		const image = this;

		// check proportion and add class
		if(image.width / image.height > 1.2) {
			image.classList.add("image-landscape");
		} else {
			image.classList.add("image-portrait");
		}

	};





	/* ------------------------------

		Resize event

	------------------------------ */

	window.addEventListener('resize', function() {
		
		// set images and go through and resize images
		const images = document.getElementsByClassName(img_position_js);

		// go through images
		for (let i = 0; i < images.length; i++) {
			// set the position only if image has loaded
			if(images[i].classList.contains(img_loaded)) {
				imageSetPosition.call(images[i]);
			}
		}

	}, false);





});