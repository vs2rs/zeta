<?php

/* --------------------------------- *\
 
	Menu

\* --------------------------------- */

// html list of our menu items
$menu_html = '';

foreach ($pages as $page_url => $menu_item) {

	// check if show in menu
	if(isset($menu_item->show_in_menu)
	&& $menu_item->show_in_menu != true) {
		continue;
	}

	// active var
	$active = '';

	// check if active page
	if($menu_item->template == $page) {
		$active = ' class="nav__active"';
	}

	// get the html template
	$menu_html .= get_html(array(

		// menu
		'name'		=> $menu_item->name,
		'link'		=> ZetaData::get_lang_url($menu_item),
		'active'	=> $active,

		// template
		'template'	=> APP_VIEWS . '/_global/nav-item.html'

	));

}





/* --------------------------------- *\
 
	Lang

\* --------------------------------- */

// html list of our menu items
$lang_html = '';

foreach ($pages as $lang_url => $lang_items) {

	// active var
	$active = '';

	// check if active page
	if($lang_url == $lang) {
		$active = '  lang__active';
	}

	// get the html template
	$lang_html .= get_html(array(

		// menu
		'lang_url'	=> $lang_url != Config::read('lang_default') ? $lang_url : APP_URL,
		'lang'		=> $lang_url,
		'active'	=> $active,

		// template
		'template'	=> APP_VIEWS . '/_global/lang-item.html'

	));

}





/* --------------------------------- *\
 
	Header output

\* --------------------------------- */

echo get_html(array(

	// navigation
	'nav'		=> $menu_html,
	'lang'		=> $lang_html,
	'page'		=> $page,
	'home_url'	=> $lang != Config::read('lang_default') ? APP_URL . '/' . $lang : APP_URL,

	// template
	'template'	=> APP_VIEWS . '/_global/header.html'

));





// -- _header.php