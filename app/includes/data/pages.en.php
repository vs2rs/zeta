<?php

// resutls
$items = array();

/* --------------------------------- *\
 
	Home

\* --------------------------------- */

$key = 'home';
$items[$key] = new stdClass();
$items[$key]->name 			= 'Home';
$items[$key]->alias 		= 'home';
$items[$key]->template 		= 'home';
$items[$key]->show_in_menu 	= true;

// extra content
$items[$key]->title = 'Digital Solutions<span class="line-break"></span>
for Industrial Businesses</h1>';
$items[$key]->hashtags = array('#production planning and scheduling', '#resource optimization', '#material optimization', '#CAD/BIM integration', '#energy efficiency');
$items[$key]->austra = '
	<svg class="home-austra__logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 254 66" xml:space="preserve"> <path d="M0,0v66l217.5,0L254,0H0z M32.4,40.4l-1.5-2.9l1.5-2.9l1.5,2.9L32.4,40.4z M42.4,48.2l-10-25.6l-10,25.6H20l12.4-30.5 l12.4,30.5H42.4z M75.5,37.3c0,7.2-4.2,11.5-10.7,11.5c-6.6,0-10.6-4.1-10.6-11.5V17.8l2.2,2.1v17.4c0,6.2,3.3,9.4,8.4,9.4 s8.5-3.3,8.5-9.4V20l2.2-2.1V37.3z M95.3,48.5c-2.9-0.1-5.8-1-8.2-2.8l1.9-1.4c1.9,1.3,4.2,2.1,6.5,2.1c3.8,0,6.3-2.6,6.3-5.9 c0-3.5-2.2-5-6.3-7c-4.4-2.1-7.1-4.2-7.1-8.4c0-4.4,3.5-7.9,8.5-7.9c2.3,0,4.5,0.7,6.4,2.1l-1.8,1.4c-1.4-0.9-3.1-1.4-4.8-1.4 c-3.8,0-6.2,2.5-6.2,5.6c0,3.3,2.4,4.8,5.9,6.5v-0.1c4.9,2.4,7.5,4.4,7.5,8.7C104,45.1,100.1,48.5,95.3,48.5z M135.4,19.8h-10.7 v28.5h-2.2V19.8h-10.7v0l2.1-2.1h19.4L135.4,19.8L135.4,19.8z M163,48.2L154.2,34c4.9-0.4,7.6-3.2,7.6-7.3c0-4.1-2.8-7.1-8.2-7.1 c-1.4,0-2.8,0.1-4.1,0.4v28.3h-2.2V18c2-0.3,4.1-0.5,6.1-0.5c7.1,0,10.8,3.7,10.8,9.1c0,4.2-2.4,7.5-6.6,8.8l8.2,12.9H163z M188.3,40.4l-1.5-2.9l1.5-2.9l1.5,2.9L188.3,40.4z M198.4,48.2l-10-25.6l-10,25.6H176l12.4-30.5l12.4,30.5H198.4z"/> </svg>

	<h2 class="text  title-default  text-strong  margin-v">
		Austra ERP is a <span class="color-main-text">modern, flexible<span class="line-break"></span>
		and comprehensive solution</span> for<span class="line-break"></span>
		industrial businesses
	</h2>

	<a href="austra" class="button  button--dark  button--inline  button--small">
		Learn more
	</a>
';
$items[$key]->austra_img = new stdClass();
$items[$key]->austra_img->src = 'uploads/pages/austra-main.png';
$items[$key]->austra_img->alt = '';
$items[$key]->cta_title = 'Unimpressed with general-purpose platforms?';
$items[$key]->cta_text = 'Perhaps we can help!';
$items[$key]->know_how_name = 'Know - How';
$items[$key]->know_how_title = 'They say the team is 50% of project<span class="line-break"></span>
success - <span class="color-comp-text">let\'s meet!</span>';
$items[$key]->know_how_button = 'Schedule A Meeting';

// meta demo
$items[$key]->meta = new stdClass();
$items[$key]->meta->title = 'Digital Solutions for Industrial Businesses | Zeta Industry';
// $items[$key]->meta->description = '';
// $items[$key]->meta->keywords = '';
$items[$key]->facebook = new stdClass();
$items[$key]->facebook->title = 'Zeta Industry';
$items[$key]->facebook->description = 'Digital Solutions for Industrial Businesses';
// $items[$key]->facebook->image = 'https://';
// $items[$key]->facebook->image_width = '1200';
// $items[$key]->facebook->image_height = '630';
// $items[$key]->facebook->url = APP_URL;
// $items[$key]->facebook->type = 'website';

/* --------------------------------- *\
 
	Austra

\* --------------------------------- */

$key = 'austra';
$items[$key] = new stdClass();
$items[$key]->name 			= 'Austra';
$items[$key]->alias 		= 'austra';
$items[$key]->template 		= 'austra';
$items[$key]->show_in_menu 	= true;

// extra content
$items[$key]->title = 'A platform that excels at industry-specific functional areas where most general-purpose solutions under-deliver';
$items[$key]->button_text = 'Apply For A Demo';
$items[$key]->meta = new stdClass();
$items[$key]->facebook = new stdClass();

/* --------------------------------- *\
 
	Use-Cases

\* --------------------------------- */

$key = 'use-cases';
$items[$key] = new stdClass();
$items[$key]->name 			= 'Use Cases';
$items[$key]->alias 		= 'use-cases';
$items[$key]->template 		= 'use_cases';
$items[$key]->show_in_menu 	= true;
$items[$key]->summary 		= 'Industries that benefit from Austra ERP';
$items[$key]->view_all 		= 'View All<br/>Projects';
$items[$key]->title 		= 'Use Cases';
$items[$key]->subtitle 		= '<strong>Industries:</strong> Metalworking, Woodworking, Construction Materials, Polygraphy Cosmetics,<span class="line-break"></span> Printing and Packaging, Pharmaceutical Products & Others';
$items[$key]->meta = new stdClass();
$items[$key]->facebook = new stdClass();


/* --------------------------------- *\
 
	Team

\* --------------------------------- */

$key = 'team';
$items[$key] = new stdClass();
$items[$key]->name 			= 'Team';
$items[$key]->alias 		= 'team';
$items[$key]->template 		= 'team';
$items[$key]->show_in_menu 	= true;
$items[$key]->title 		= 'Team';
$items[$key]->subtitle 		= 'Industry expertise, communication and technical skills for smooth project implementation';
$items[$key]->join_title 	= 'Want to join our team?';
$items[$key]->join_subtitle = 'We are always looking for collegues. If you see how you can<span class="line-break"></span> add value to our company, lets talk!';
$items[$key]->join_button 	= 'Get in touch';
$items[$key]->meta = new stdClass();
$items[$key]->facebook = new stdClass();

/* --------------------------------- *\
 
	Contacts

\* --------------------------------- */

$key = 'contacts';
$items[$key] = new stdClass();
$items[$key]->name 			= 'Contacts';
$items[$key]->alias 		= 'contacts';
$items[$key]->template 		= 'contacts';
$items[$key]->show_in_menu 	= true;
$items[$key]->title 		= 'Offices & Partners';
$items[$key]->subtitle 		= 'Get in touch with us about solutions, partnerships, and networking';
$items[$key]->footer_title 	= 'Digital Solutions<br/>for Industrial Businesses';
$items[$key]->follow 		= 'Follow Us';
$items[$key]->modules 		= 'AUSTRA Modules';
$items[$key]->makit 		= 'Zeta Industry is a part of<br/><a class="color-main-text" href="https://www.makit.lv/" target="_blank">MAK IT group</a>';
$items[$key]->copyright 	= 'Zeta industry, Inc. © 2018-';
$items[$key]->block_title 	= 'Contact Us to Learn More <span class="color-comp-text">
How AUSTRA Can Help</span> Your Industry';
$items[$key]->block_button 	= 'Request a Demo';
$items[$key]->meta = new stdClass();
$items[$key]->facebook = new stdClass();

/* --------------------------------- *\
 
	Privacy Policy

\* --------------------------------- */

$key = 'privacy-policy';
$items[$key] = new stdClass();
$items[$key]->name 			= 'Privacy Policy';
$items[$key]->alias 		= 'privacy-policy';
$items[$key]->template 		= 'privacy';
$items[$key]->show_in_menu 	= false;
$items[$key]->title 		= 'Privacy Policy';
$items[$key]->subtitle 		= '';
$items[$key]->cookies 		= 'Please be informed that cookies are used on this website . By continuing to use the site, you agree to use of cookies.';
$items[$key]->more 			= 'Find out more!';
$items[$key]->agree 		= 'I agree';
$items[$key]->meta = new stdClass();
$items[$key]->facebook = new stdClass();





// -- pages.en.php