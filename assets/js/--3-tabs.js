/* --------------------------------- *\

	Tabs in Austra section
	
\* --------------------------------- */

document.addEventListener('DOMContentLoaded', function() {

	// get the modules element
	const modules_menu = getElementByClass('.modules__wrap');
	if(modules_menu) {

		// get the menu items
		const modules = document.querySelectorAll('.modules__wrap > div');
		modules.forEach((item) => {

			// on click
			item.addEventListener('click', function() {

				// remove active class
				modules.forEach((item) => item.classList.remove('modules__active'));

				// add active class
				item.classList.add('modules__active');

				// get base href and change url
				const base_href = document.querySelector("base").href;
				window.history.pushState({}, '', base_href + item.dataset.url);

				// get all the content and remove active
				const modules_content = document.querySelectorAll('.modules-content > div');
				modules_content.forEach((module_content) => {
					module_content.classList.remove('modules-content__active');
				});

				// get the current content that needs to be set active
				const id = this.dataset.id;
				const content = getElementByClass('.modules-content [data-id="'+id+'"]');

				// add active class
				content.classList.add('modules-content__active');

				// need to update menu in the footer
				const footer_modules = document.querySelectorAll('.footer__modules a');
				footer_modules.forEach((footer_item) => {
					if(footer_item.dataset.id == id) {
						footer_item.classList.remove('color-main-text');
						footer_item.classList.add('color-dark-text');
					} else {
						footer_item.classList.add('color-main-text');
						footer_item.classList.remove('color-dark-text');
					}
				});

			});

		});

	}

	// get the modules element
	const tabs_menu = getElementByClass('.austra-tabs__menu');
	if(tabs_menu) {

		// get the menu items
		const buttons = tabs_menu.querySelectorAll('.button');
		buttons.forEach((button) => {

			// on click
			button.addEventListener('click', function() {

				// remove active class
				buttons.forEach((item) => item.classList.remove('austra-tabs__menu__active'));

				// add active class
				button.classList.add('austra-tabs__menu__active');

				// get all the content and remove active
				const tabs = document.querySelectorAll('.austra-tabs__tab');
				tabs.forEach((tab) => {
					tab.classList.remove('austra-tabs__active');
				});

				// get the current content that needs to be set active
				const id = this.dataset.tab;
				const content = getElementByClass('.austra-tabs__tab[data-tab="'+id+'"]');

				// if modules tab
				if(id == 1) {
					
					// get the modules
					const modules = document.querySelectorAll('.modules-content > div');
					const modules_menu = document.querySelectorAll('.modules__wrap > div');
					
					// remove active class
					modules.forEach((item) => item.classList.remove('modules-content__active'));
					modules_menu.forEach((item) => item.classList.remove('modules__active'));

					// set 0 tab to active
					const tab_0 = document.querySelector('.modules-content > div[data-id="0"]');
					tab_0.classList.add('modules-content__active');

				}

				// add active class
				content.classList.add('austra-tabs__active');

				// get base href and change url
				const base_href = document.querySelector("base").href;
				window.history.pushState({}, '', base_href + button.dataset.url);

				// need to update menu in the footer
				const footer_modules = document.querySelectorAll('.footer__modules a');
				footer_modules.forEach((footer_item) => {
					footer_item.classList.add('color-main-text');
					footer_item.classList.remove('color-dark-text');
				});

			});
			
		});

	}

});