<?php

// resutls
$items = array();

$key = 0;
$items[$key] = new stdClass();
$items[$key]->name = 'Skonto Concrete Cladding';
$items[$key]->url = 'skonto-concrete-cladding';
$items[$key]->web = 'https://www.skontocc.com';
$items[$key]->products = 'Glass reinforced concrete facade elements';
$items[$key]->overview = 'Implementation of Austra ERP project management and production planning modules with the key objectives: 1) To integrate engineering and manufacturing business units into a common digital ecosystem; 2) To benefit from Austra ERP automation capabilities. The platform allowed direct element list, drawing file, and revision information upload from the CAD system. The uploaded element attributes were in turn used by the automation tools that take care of keeping production schedules optimal and making adjustments when project delivery dates shifted. A tailored algorithm created an optimal element mold form plan to deliver projects on time with least possible resource utilization.';
$items[$key]->modules = array(
	'Product management',
	'Production planner',
	'Mobile application'
);
$items[$key]->integration = array(
	'SAP (Financial management)',
	'Tekla Structures (design and engineering)'
);
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/skonto-concrete-cladding.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/skonto-concrete-cladding.png';
$items[$key]->logo->alt = '';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'AM Furnitūra';
$items[$key]->url = 'am-furnitura';
$items[$key]->web = 'https://www.amf.lv/en/';
$items[$key]->products = 'Furniture fittings - lighting systems, closets, lifting and drawer mechanisms, sliding systems';
$items[$key]->overview = 'Austra ERP was implemented for production planning and order management. Multiple integrations were introduced with existing business platforms for finance, equipment management, e-commerce solutions. One of the main goals of the project was to make Austra ERP the central integrator of digital systems used by the company. Austra ERP was adapted to suit the particular needs of their aluminum manufacturing and woodworking manufacturing facility, and both were integrated within the same system. With Austra ERP AM Frunitūra can manage all their orders and manufacture within the same platform, which communicates with other digital systems they use via integrations.';
$items[$key]->modules = array(
	'Sales, estimation and order management',
	'Product configurator',
	'Production planner'
);
$items[$key]->integration = array(
	'Microsoft NAV (financial management)',
	'CutRite (material optimization)'
);
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/am-furnitura.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/am-furnitura.png';
$items[$key]->logo->alt = '';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Ripo International';
$items[$key]->url = 'ripo-international';
$items[$key]->web = 'https://ripo-baltic.com/en';
$items[$key]->products = 'Sectional window and door systems, insect screens, roller shutters';
$items[$key]->overview = 'Before Austra ERP, the company\'s employees used to spend a significant amount of time on order estimates and BOM calculations. Since the products were complex and consisted of more than 1000 parts, the manufacturing process lacked transparency and predictability. To solve this, Austra ERP was implemented for full cycle order management, from estimation to assembly. The platform allowed to fully automate order estimates, generate the relevant documentation, and initiate manufacturing resource capacity-based planning. Austra ERP was integrated with an e-commerce solution and financial accounting platform, and provided a dedicated interface for distribution partners to make orders, receive instant quotes, and review delivery statuses.';
$items[$key]->modules = array('Sales, estimation and order management', 'Product configurator', 'Production planner', 'Assembly planner');
$items[$key]->integration = array('Horizon (financial management)');
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/ripo-international.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/ripo-international.png';
$items[$key]->logo->alt = '';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Tenax Group';
$items[$key]->url = 'tenax-group';
$items[$key]->web = 'https://tenapors.lv/en/';
$items[$key]->products = 'Polystyrene products and solutions - insulation panels, composite finishing solutions, insulated concrete forms, and more.';
$items[$key]->overview = 'Austra ERP was implemented for several Tenax group companies like Tenapors, Tenax Panel and Tenax Install. The purpose of the project was to create a unified digital infrastructure for the performance of all essential business processes - sales, information processing, order management, production planning, logistics and assembly. During the project, several automation algorithms were introduced for production planning, package assembly and delivery planning. These significantly reduced time spent on planning. Austra ERP also improved the flow of information for both normal business processes and allowed for sudden changes in planning when necessary to be done swiftly, thus reducing overhead costs, and improving the adaptability of the production process at all levels.';
$items[$key]->modules = array(
	'Sales, estimation and order management',
	'Product configurator',
	'Production planner',
	'Logistics management',
	'Quality management',
	'Assembly planning'
);
$items[$key]->integration = array(
	'Hansaworld (financial management)'
);
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/tenax-group.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/tenax-group.png';
$items[$key]->logo->alt = '';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'A.C.B.';
$items[$key]->url = 'skonto-acb';
$items[$key]->web = 'https://www.acb.lv/?lang=en';
$items[$key]->products = 'Road Construction';
$items[$key]->overview = 'Austra ERP was set up for management, coordination and operational planning of Group\'s asphalt production unit network. The goal of the project was creating a transparent and efficient digital platform to navigate the complexity of the Company\'s business - road construction - with shifting construction site work schedules, limited product life-cycles, and other constraining factors.';
$items[$key]->modules = array(
	'Project management',
	'Sales, estimation and order management',
	'Production planner'
);
$items[$key]->integration = array(
	'Horizon (Financial management)'
);
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/skonto-acb.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/skonto-acb.png';
$items[$key]->logo->alt = '';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Alan Deko';
$items[$key]->url = 'alan-deko';
$items[$key]->web = 'https://alandeko.com/en/';
$items[$key]->products = 'Interior design element manufacturing';
$items[$key]->overview = 'Alan Deko were planning production and installation via spreadsheets that took a lot of manual work. The main goal of the project was to automate production and installation planning. With Austra ERP order input was simplified and automatic manufacturing planning was implemented. Warehouse management and templates for purchase orders were introduced, in order to reduce time spent on replenishing stock.';
$items[$key]->modules = array(
	'Sales, estimation and order management',
	'Production planner',
	'Warehouse management'
);
$items[$key]->integration = array(
	'Integra'
);
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/alan-deko.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/alan-deko.png';
$items[$key]->logo->alt = '';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'PMTM';
$items[$key]->url = 'pmtm';
$items[$key]->web = 'https://pmtm.lv/en';
$items[$key]->products = 'Large format printing/UV printing';
$items[$key]->overview = 'Austra ERP was implemented to simplify the input of orders, and automate production planning. A dynamic product configurator was introduced that takes into account all available dependencies, and ensures that the correct materials and processing is only available where applicable, thus enabling a much simpler input of orders, and significantly reducing errors in manufacturing orders. Calculations of manufacturing capacities and raw material expenditure, were automatized by retrieving operational information from their printers, thus simplifying production planning and reporting.';
$items[$key]->modules = array(
	'Sales, estimation and order management',
	'Production planner',
	'Warehouse management'
);
$items[$key]->integration = array(
	'Horizon'
);
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/pmtm.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/pmtm.png';
$items[$key]->logo->alt = '';

$key++;
$items[$key] = new stdClass();
$items[$key]->name = 'Ette Tete';
$items[$key]->url = 'ette-tete';
$items[$key]->web = 'https://ettetete.com/';
$items[$key]->products = 'Convertible helper towers and climbing frames for children';
$items[$key]->overview = 'Austra ERP was implemented for streamlined production management and planning. Main goal of the project was to provide the company with a system that receives orders from an external CRM and automatically plans manufacturing. We developed an algorithm that takes into account sequential operations with adjustable offsets and differing capacities of production devices at various stages of production, thus significantly reducing the amount of work necessary to plan manufacturing. Warehouse management was also implemented, and the manufacturing planning algorithm takes into account the set minimum and maximum of goods, and plans production accordingly.';
$items[$key]->modules = array(
	'Production planner',
	'Sales, estimation and order management',
	'Warehouse management'
);
$items[$key]->integration = false;
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/use_cases/ette-tete.jpg';
$items[$key]->img->alt = '';
$items[$key]->logo = new stdClass();
$items[$key]->logo->src = 'uploads/use_cases/ette-tete.png';
$items[$key]->logo->alt = '';





// -- use.cases.en.php