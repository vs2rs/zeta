<?php

/**
 * ------------------------------------
 * Split URL function
 * @param $url - url to split
 * @param $remove - part to remove
 * ------------------------------------
 */

function split_url($url, $remove = false) {

	// default is empty array
	$result = array();

	// remove query string
	list($url) = explode('?', $url);

	// remove slashes from begining and end
	$url = preg_replace(array("/^\//", "/\/$/"), '', $url);

	// check if we need to remove somting from the url
	if($remove) {

		// remove trailing slash
		$remove = preg_replace(array("/^\//", "/\/$/"), '', $remove);

		// remove it from the url
		$url = str_replace($remove, '', $url);

	}

	// setupu
	foreach(explode('/', $url) as $part) {
		if(strlen($part) > 0) $result[] = $part;
	}

	return $result;

}





// -- function.split_url.php