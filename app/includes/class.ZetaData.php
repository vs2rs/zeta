<?php

/**
 * Static class for all the content on the page
 * @author vs2rs
*/

class ZetaData {

	/**
	 * Pages
	 * @return array
	*/
	public static function get_pages($page = false) {

		// get lang
		$lang = Config::read('lang_current');

		// load data
		require("data/pages.$lang.php");

		// return a single page if set
		if($page) {
			if(isset($items[$page])) return $items[$page];
			else return false;
		}

		// return data
		return $items;
		
	}

	/**
	 * Tabs
	 * @return array
	*/
	public static function get_tabs($tab = false) {

		// get lang
		$lang = Config::read('lang_current');

		// load data
		require("data/tabs.$lang.php");

		// return a single page if set
		if($tab) {
			if(isset($items[$tab])) return $items[$tab];
			else return false;
		}

		// return data
		return $items;
		
	}

	/**
	 * Get Data File
	 *
	 * @return array
	*/
	public static function get_data_file($file, $limit = false) {

		// get lang
		$lang = Config::read('lang_current');

		// load data
		require("data/$file.$lang.php");

		// if limit set
		if(isset($limit) && $limit) {
			$items = array_slice($items, 0, $limit);
		}

		// return data
		return $items;

	}

	/**
	 * Get Data File: Know-How
	 *
	 * @return array
	*/
	public static function know_how($limit = false) {
		return self::get_data_file('know.how', $limit);
	}

	/**
	 * Get Data File: Automations
	 *
	 * @return array
	*/
	public static function automations($limit = false) {
		return self::get_data_file('automations', $limit);
	}

	/**
	 * Get Data File: Presentations
	 *
	 * @return array
	*/
	public static function presentations($limit = false) {
		return self::get_data_file('presentations', $limit);
	}

	/**
	 * Get Data File: Modules
	 *
	 * @return array
	*/
	public static function modules($limit = false) {
		return self::get_data_file('modules', $limit);
	}

	/**
	 * Get Data File: Use Cases
	 *
	 * @return array
	*/
	public static function use_cases($limit = false) {
		return self::get_data_file('use.cases', $limit);
	}

	/**
	 * Get Data File: Team
	 *
	 * @return array
	*/
	public static function team($limit = false) {
		return self::get_data_file('team', $limit);
	}

	/**
	 * Get Data File: Offices
	 *
	 * @return array
	*/
	public static function offices($limit = false) {
		return self::get_data_file('offices', $limit);
	}

	/**
	 * Setup url for given language
	 * @param page = current page object
	 * @return string
	 */
	public static function get_lang_url($page, $lang = false) {

		// no param in the url by default
		$lang_url = '';

		// if not set use current
		if(!$lang) $lang = Config::read('lang_current');

		// if not default add lang to the url
		if($lang != Config::read('lang_default')) {
			$lang_url = $lang;
		}

		// check if we need to check for page
		if($page->template != Config::read('homepage')) {
			$lang_url .= ( $lang_url != '' ? '/' : '' ) . $page->alias;
		}

		// if default lang and page set to the app url
		if($lang == Config::read('lang_default')
		&& $page->template == Config::read('homepage')) {
			$lang_url = APP_URL;
		}

		// return url
		return $lang_url;

	}

}




