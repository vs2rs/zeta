<?php

// set the array
$config = array();

/* --------------------------------- *\
 
	Some defaults for content

\* --------------------------------- */

// language setup
$config["lang_current"] = "en";
$config["lang_default"] = "en";
$config["languages"] 	= array("en", "lv");
$config["homepage"] 	= 'home';

/* --------------------------------- *\
 
	Global e-mail settings

\* --------------------------------- */

// send mail through smtp
$config["email_smtp"] 	= true;

// setup smtp
$config["email_host"]	= 'mail.server';
$config["email_port"]	= 465;
$config["email_secure"]	= 'ssl';
$config["email_auth"]	= true;
$config["email_user"]	= 'user@email';
$config["email_pass"]	= 'pass';

// show info in the error log
$config["email_debug"] 	= false; // 1, 2 or 3

// email to/from info for contact form
$config["email_to"] 	= array("viesturs@goodtimes.lv");
$config["email_from"] 	= "admin@goodtimes.lv";
$config["email_name"] 	= 'Zeta Industry';
$config["email_subject"] = 'Aizpildīta kontaforma mājaslapā www.zetaindustry.lv';