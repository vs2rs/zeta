<?php

// template
$email = file_get_contents('email.html');

// logo
$row = str_replace(
	'{{url}}',
	'https://localhost/makit/zeta',
	file_get_contents('email-logo.html')
);

$row .= str_replace(
	array('{{label}}', '{{value}}', '{{value_size}}'),
	array('Kontaktpersona', 'Name Surname</br>email@address.com', '16px'),
	file_get_contents('email-form-row.html')
);

$row .= str_replace(
	array('{{label}}', '{{value}}', '{{value_size}}'),
	array('Ziņojums', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel cursus ipsum. Aenean sit amet interdum est, et vestibulum mauris. Praesent est ante, feugiat eget sollicitudin eu, tempus non eros. Vestibulum mollis nisl a nisl placerat, id mollis sapien mollis. Phasellus faucibus hendrerit laoreet.', '16px'),
	file_get_contents('email-form-row.html')
);

// output
echo str_replace(
	array('{{content}}', '{{url}}'),
	array($row, 'https://localhost/makit/zeta'),
	$email
);