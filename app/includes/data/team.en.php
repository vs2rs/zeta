<?php

// resutls
$items = array();

$key = 0;
$items[$key] = new stdClass();
$items[$key]->name = 'Olga Rinke';
$items[$key]->position = 'Head of Sales';
$items[$key]->linkedin = 'https://www.linkedin.com/in/olga-%C4%BCes%C5%86ikovi%C4%8Da-ri%C5%86%C4%B7e-60b858120/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-olga.jpg';
$items[$key]->img->alt = 'Olga Rinke';

$key = 1;
$items[$key] = new stdClass();
$items[$key]->name = 'Edgars Augstkans';
$items[$key]->position = 'Partner / Product Owner';
$items[$key]->linkedin = 'https://www.linkedin.com/in/edgars-augstkalns-661a0771/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-edgars.jpg';
$items[$key]->img->alt = 'Edgars Augstkans';

$key = 2;
$items[$key] = new stdClass();
$items[$key]->name = 'Anna Bičkova';
$items[$key]->position = 'Process Analyst and Project Manager';
$items[$key]->linkedin = 'https://www.linkedin.com/in/anna-bickova-31170526/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-anna.jpg';
$items[$key]->img->alt = 'Anna Bičkova';

$key = 3;
$items[$key] = new stdClass();
$items[$key]->name = 'Arturs Seilis';
$items[$key]->position = 'Partner / Business Development';
$items[$key]->linkedin = 'https://www.linkedin.com/in/arturs-seilis-9a192a3b/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-arturs-1.jpg';
$items[$key]->img->alt = 'Arturs Seilis';

$key = 4;
$items[$key] = new stdClass();
$items[$key]->name = 'Arvis Zeile';
$items[$key]->position = 'Partner / IT Architect';
$items[$key]->linkedin = 'https://www.linkedin.com/in/arvis-zeile-7854403/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-arvis.jpg';
$items[$key]->img->alt = 'Arvis Zeile';

$key = 5;
$items[$key] = new stdClass();
$items[$key]->name = 'Niks Berzins';
$items[$key]->position = 'Process Analyst and Project Manager';
$items[$key]->linkedin = 'https://www.linkedin.com/in/niks-berzins-2b0b0444/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-niks.jpg';
$items[$key]->img->alt = 'Niks Berzins';

$key = 6;
$items[$key] = new stdClass();
$items[$key]->name = 'Davis Namsons';
$items[$key]->position = 'CTO';
$items[$key]->linkedin = 'https://www.linkedin.com/in/davis-namsons/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-davis.jpg';
$items[$key]->img->alt = 'Davis Namsons';

$key = 7;
$items[$key] = new stdClass();
$items[$key]->name = 'Austris Landmanis';
$items[$key]->position = 'Software engineer';
$items[$key]->linkedin = 'https://www.linkedin.com/in/austris-landmanis/';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-austris.jpg';
$items[$key]->img->alt = 'Austris Landmanis';

$key = 8;
$items[$key] = new stdClass();
$items[$key]->name = 'Oskars Segrums';
$items[$key]->position = 'Software engineer';
$items[$key]->linkedin = false;
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-oskars.jpg';
$items[$key]->img->alt = 'Oskars Segrums';

$key = 9;
$items[$key] = new stdClass();
$items[$key]->name = 'Artūrs Cibulis';
$items[$key]->position = 'Software engineer';
$items[$key]->linkedin = false;
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/team/team-arturs-2.jpg';
$items[$key]->img->alt = 'Artūrs Cibulis';





// -- team.en.php