<?php

/* --------------------------------- *\
 
	Team

\* --------------------------------- */

// template
echo get_html(array(

	// content
	'intro'		=> ZetaTemplates::intro_simple('team'),
	'team'		=> ZetaTemplates::team(),
	'team_join'	=> ZetaTemplates::team_join(),
	'contacts' 	=> ZetaTemplates::block_contacts(),

	// template
	'template'	=> APP_VIEWS . '/team/team.html'

));





// -- __team.php