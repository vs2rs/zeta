<?php

// resutls
$items = array();

/* --------------------------------- *\
 
	About

\* --------------------------------- */

$key = 'about';
$items[$key] = new stdClass();
$items[$key]->name = 'About Austra';
$items[$key]->alias = 'about-austra';
$items[$key]->template = 'austra-tab-about.html';
$items[$key]->info_1 = '
<ul>
	<li><strong>Fully web-based:</strong> accessible from anywhere</li>
	<li><strong>A modular platform:</strong> compiled according to the business unit infrastructure</li>
	<li><strong>Automation capabilities:</strong> Production scheduling and resource and material optimization</li>
	<li><strong>Synchronized and transparent</strong> process management</li>
	<li><strong>Extensive and tailored</strong> features for manufacturers</li>
	<li>Entirely based on <strong>Open Source</strong> technologies</li>
</ul>
';
$items[$key]->info_1_img = new stdClass();
$items[$key]->info_1_img->src = 'uploads/pages/austra-info-1.jpg';
$items[$key]->info_1_img->alt = '';
$items[$key]->info_2 = '
	Intuitive, structured<br/>
	and <span class="color-main-text">color-coded<br/>
	interfaces</span>
';
$items[$key]->info_2_img = new stdClass();
$items[$key]->info_2_img->src = 'uploads/pages/austra-info-2.png';
$items[$key]->info_2_img->alt = '';
$items[$key]->info_3_title = 'Automation capabilities';
$items[$key]->info_3_subtitle = 'Austra ERP is built to take advantage of automated processes. Our algorithms automate:';
$items[$key]->info_4 = '
	<h2 class="text  title-large  text-strong  margin-b">
		Licensing models <span class="color-main-text">that<span class="line-break"></span>
		facilitate growth</span>
	</h2>

	<div class="text  text-default">
		Be it an increase of workforce, establishment of a new production facility or expansion into export markets - the digital infrastructure costs will not be a burden and remain almost constant.
	</div>
';
$items[$key]->info_4_img = new stdClass();
$items[$key]->info_4_img->src = 'uploads/pages/austra-info-4.png';
$items[$key]->info_4_img->alt = '';

/* --------------------------------- *\
 
	Modules

\* --------------------------------- */

$key = 'modules';
$items[$key] = new stdClass();
$items[$key]->name = 'Modules';
$items[$key]->alias = 'modules';
$items[$key]->template = 'austra-tab-modules.html';
$items[$key]->title = '
	A flexible and<br/>
	<span class="color-main-text">integrated modular infrastructure</span>
	- tailored to your business
';
$items[$key]->img = new stdClass();
$items[$key]->img->src = 'uploads/pages/austra-main.png';
$items[$key]->img->alt = '';

/* --------------------------------- *\
 
	Presentations

\* --------------------------------- */

$key = 'presentations';
$items[$key] = new stdClass();
$items[$key]->name = 'presentations';
$items[$key]->alias = 'presentations';
$items[$key]->template = 'austra-tab-presentations.html';
$items[$key]->title = 'Platform Presentations';
$items[$key]->subtitle = 'Explore to learn more about Austra ERP technical capabilities,<span class="line-break"></span> industry-specific versions, and more';
$items[$key]->button = 'Explore all presentations';





// -- tabs.en.php