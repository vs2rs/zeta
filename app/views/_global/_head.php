<?php

/* --------------------------------- *\
 
	Head output

\* --------------------------------- */

// some defaults
Config::write('site_name', 'Zeta Industry');
Config::write('meta_author', 'Zeta Industry');

// get the page
$page_current = ZetaData::get_pages($page_id);

// default
$meta = get_meta($page_current);
$page_title = get_meta_title($page_current);

echo get_html(array(

	// params
	'title'		=> $page_title,
	'base'		=> APP_URL,
	'meta'		=> $meta,

	// css and js
	'css'		=> get_header_element('css'),
	'js'		=> get_header_element('js'),

	// template
	'template'	=> APP_VIEWS . '/_global/head.html'

));





// -- _head.php