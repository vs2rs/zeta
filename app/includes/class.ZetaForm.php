<?php

/**
 * Static class for input fields
 * @author vs2rs
*/

class ZetaForm extends Form {

	/**
	 * Inputs
	 *
	 * @return array
	*/
	public static function inputs($lang) {

		return array(

			array(
				// define input type
				'input_type'	=> 'text',
				// values
				'name' 			=> 'zeta_name',
				'label'			=> self::lang($lang, 'name_label'),
				'placeholder' 	=> '',
				'color' 		=> false,
				// validation
				'required' 		=> true,
				'length'		=> 50,
				'validate'		=> 'letters_utf8'
			),

			array(
				// define input type
				'input_type'	=> 'input_email',
				// values
				'name' 			=> 'zeta_email',
				'label'			=> self::lang($lang, 'email_label'),
				'placeholder' 	=> '',
				// validation
				'required' 		=> true,
				'length'		=> 128,
				'validate'		=> 'email',
			),

			array(
				// define input type
				'input_type'	=> 'textarea',
				// values
				'name' 			=> 'zeta_text',
				'label'			=> self::lang($lang, 'message_label'),
				'placeholder' 	=> self::lang($lang, 'message_label'),
				// validation
				'required' 		=> true,
				'validate'		=> 'text'
			)

		);

	}

	/**
	 * Send the form
	 * @param $input_post_data - array - our post data
	 * @param $params - array - params for our send mail function
	 * @return void
	*/
	public static function send_contact_form($input_post_data, $params) {

		// setup our email template
		$email_template = self::email_template($input_post_data);
		$email_attachments = self::create_file_list($input_post_data);

		// send the mail
		$email_sent = send_mail(
			$params,
			$email_template,
			$email_attachments
		);

		// default to error
		$redirect_url = '?send=error#form';

		// check if email sent
		if($email_sent) {
			$redirect_url = '?send=success#form';
		}

		// get lang and pages
		$lang = Config::read('lang_current');
		if($lang != Config::read('lang_default')) {
			$redirect_url = $lang . $redirect_url;
		}

		// redirect
		header('Location: ' . $redirect_url); exit;

	}

	/**
	 * Some language stuff
	 * @param $lang - string - lang code
	 * @return string
	*/
	public static function lang($lang, $param) {

		// params
		$params = array(

			"en" => array(

				"form_title" 	=> 'Contact Us to Learn More <span class="color-comp-text">
									How AUSTRA Can Help</span> Your Industry',

				"name_label" 	=> "Name",
				"email_label" 	=> "E-mail",
				"message_label" => "Message",
				"submit_button"	=> "Request a Demo",

				"form_success"	=> "Thank You, your message has been successfully sent!",
				"send_error"	=> "Error sending the form! Try again later or contact us directly."

			),

			"lv" => array(

				"form_title" 	=> 'Sazinies ar mums, lai uzzinātu <span class="color-comp-text">
									kā AUSTRA var palīdzēt</span> jūsu sfērā',

				"name_label" 	=> "Vārds",
				"email_label" 	=> "E-pasts",
				"message_label" => "Ziņojums",
				"submit_button"	=> "Nosūtīt ziņu",

				"form_success"	=> "Paldies, jūsu ziņa veiksmīgi nosūtīta mums!",
				"send_error"	=> "Kļūda nosūtot formu! Mēģiniet lūdzu vēlāk vēlreiz."

			)

		);

		// return
		return $params[$lang][$param];

	}

	/**
	 * Forest Value E-mail Form
	 * @param array $inputs - submited inputs
	*/
	public static function email_template($inputs) {

		// set the template
		$email_template = '';

		// get the logo
		$email_template .= get_html(array(
			'url'		=> 'https://zeta.goodtimes.lv',
			'template'	=> APP_VIEWS . '/contacts/email/email-logo.html'
		));

		$email_contact = '';
		$email_company = '';

		// go through inputs
		foreach ($inputs as $input) {

			// ignore files and privacy checkbox
			if($input['input_type'] == 'input_file') continue;
			if($input['input_type'] == 'input_checkbox') continue;

			// check if value is set
			if(isset($input['value']) && $input['value'] != '') {

				// contacts
				if($input['name'] == 'zeta_name'
				|| $input['name'] == 'zeta_email') {
					$email_contact .= '<div>' . $input['value'] . '</div>';
				}

				if($input['name'] == 'zeta_text') {
					$email_message = $input['value'];
				}

			}

		}
			
		// get the row
		$email_template .= get_html(array(
			'label'			=> 'Kantakpersona',
			'value'			=> $email_contact,
			'value_size'	=> '16px',
			'template'		=> APP_VIEWS . '/contacts/email/email-form-row.html'
		));
			
		// get the row
		if(isset($email_message)) {
			$email_template .= get_html(array(
				'label'			=> 'Ziņojums',
				'value'			=> $email_message,
				'value_size'	=> '16px',
				'template'		=> APP_VIEWS . '/contacts/email/email-form-row.html'
			));
		}

		// add everythin to the template
		$email_template = get_html(array(

			// content
			'content'	=> $email_template,
			'url'		=> 'https://zeta.goodtimes.lv',

			// templates
			'template'	=> APP_VIEWS . '/contacts/email/email.html'

		));

		// return
		return $email_template;

	}

}





// -- class.SpiceAtsauksmesForm.php