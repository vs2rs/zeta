<?php

/* --------------------------------- *\
 
	APP SETUP

\* --------------------------------- */

// one dir up
define("APP_PATH", dirname(dirname(__FILE__)));
define("APP_CONFIG_FILE", dirname(__FILE__) . '/config.php');

// check if document root is set
if(isset($_SERVER['DOCUMENT_ROOT'])) {
	$document_root = $_SERVER['DOCUMENT_ROOT'];
} else {
	$document_root = false; // needs to be set manualy
	if(!$document_root) die('Document root directory not found.');
}

// get only the app folder with out the full path
define("APP_FOLDER", str_replace($document_root, "", APP_PATH));

// our HTML template folder
define("APP_VIEWS", APP_PATH . '/app/views');

// folders for assets
define("APP_CSS", '/assets/css');
define("APP_JS", '/assets/js');
define("APP_IMAGES", '/assets/images');

// do we minify the html?
define("MINIFY_HTML", false);

// define app url
if($_SERVER['SERVER_NAME'] == 'localhost') {
	define("APP_URL", '//localhost' . APP_FOLDER);
} else {
	define("APP_URL", '//zeta.goodtimes.lv' . APP_FOLDER);
}

// define app url
define("APP_SERVER", 'dev');





// -- setup.php --