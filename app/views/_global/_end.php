<?php

/* --------------------------------- *\
 
	End

\* --------------------------------- */

// privacy
$privacy = ZetaData::get_pages('privacy-policy');

// output
echo get_html(array(

	// content
	'text'	=> $privacy->cookies,
	'more'	=> $privacy->more,
	'agree'	=> $privacy->agree,
	'link'	=> ZetaData::get_lang_url($privacy),

	// template
	'template'	=> APP_VIEWS . '/_global/end.html'

));





// -- _end.php