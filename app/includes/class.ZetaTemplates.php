<?php

/**
 * Static class for templates
 * @author vs2rs
*/

class ZetaTemplates {

	/* --------------------------------- *\
 
		Presentations

	\* --------------------------------- */

	/**
	 * Presentation
	 * @return html string
	*/
	public static function presentations($limit = false, $page = false) {

		// variable
		$items_html = '';

		// get the children
		if($items = ZetaData::presentations($limit)) {

			foreach($items as $key => $item) {

				// set the number
				$number = $key + 1;
				$number = $number < 10 ? '0' . $number : $number;

				// setup html template
				$content = '
					<div class="text  text-small  margin-b">
						Presentation #' . $number . '
					</div>
					<div class="text  title-small  text-strong">
						' . $item->name . '
					</div>
					<a href="' . $item->file . '">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 22" xml:space="preserve"> <polygon points="10,14.5 0,4.5 4.3,0.2 10,5.9 15.7,0.2 20,4.5 "/> <rect x="0.6" y="17.3" width="18.8" height="4.6"/> </svg>
					</a>
				';

				// setup template
				$items_html .= get_html(array(

					// content
					'color'		=> $item->color,
					'content'	=> $content,
					'class'		=> $page && $page == 'austra' ? '  card--3-gray' : '',

					// image
					'image'			=> $item->img ? $item->img->src : false,
					'image_alt'		=> $item->img ? $item->img->alt : '',

					// template
					'template' 	=> APP_VIEWS . '/_cards/card-3.html'

				));

			}

		}

		// nothing found
		return $items_html;

	}





	/* --------------------------------- *\
 
		Austra: Modules

	\* --------------------------------- */

	/**
	 * Module Menu
	 * @return html string
	*/
	public static function module_menu($module = false) {

		// variable
		$items_html = '';
		$lang = Config::read('lang_current');

		// pages
		$page = ZetaData::get_pages('austra');
		$austra_url = ZetaData::get_lang_url($page);

		// get the children
		if($items = ZetaData::modules()) {

			foreach($items as $key => $item) {

				// set the number
				$item->id = $key + 1;

				// set active class
				$active = '';
				if($item->url == $module) {
					$active = ' class="modules__active"';
				}

				// setup template
				$items_html .= get_html(array(

					// content
					'id'		=> $item->id,
					'url'		=> $austra_url . '/modules/' . $item->url,
					'name'		=> $item->name,
					'image'		=> $item->icon ? $item->icon : false,
					'active'	=> $active,

					// template
					'template' 	=> APP_VIEWS . '/austra/modules-menu-item.html'

				));

			}

		}

		// nothing found
		return $items_html;

	}

	/**
	 * Modules content
	 * @return html string
	*/
	public static function modules($module = false) {

		// variable
		$items_html = '';

		// curent tab
		$tab = ZetaData::get_tabs('modules');

		// setup template
		$items_html .= get_html(array(

			// if no module set then intro is active
			'active'	=> !$module ? '  modules-content__active' : '',

			// content
			'title'		=> $tab->title,

			// image
			'image'		=> $tab->img ? $tab->img->src : false,
			'image_alt'	=> $tab->img ? $tab->img->alt : '',

			// template
			'template' 	=> APP_VIEWS . '/austra/modules-intro.html'

		));

		// get the children
		if($items = ZetaData::modules()) {

			// setup
			foreach($items as $key => $item) {

				// set the number
				$item->id = $key + 1;

				// set active class
				$active = '';
				if($item->url == $module) {
					$active = '  modules-content__active';
				}

				// setup template
				$items_html .= get_html(array(

					// content
					'id'		=> $item->id,
					'name'		=> $item->name,
					'content'	=> $item->content,
					'active'	=> $active,

					// image
					'image'		=> $item->img ? $item->img->src : false,
					'image_alt'	=> $item->img ? $item->img->alt : '',

					// template
					'template' 	=> APP_VIEWS . '/austra/module.html'

				));

			}

		}

		// nothing found
		return $items_html;

	}





	/* --------------------------------- *\
 
		Home: Know - How

	\* --------------------------------- */

	/**
	 * Template for Know How in the home page
	 * 
	 * @return html string
	*/
	public static function know_how() {

		// variable
		$items_html = '';

		// get the children
		if($items = ZetaData::know_how()) {

			// setup
			foreach($items as $key => $item) {

				// setup template
				$items_html .= get_html(array(

					// content
					'name'		=> $item->name,

					// image
					'image'		=> $item->img ? $item->img->src : false,
					'image_alt'	=> $item->img ? $item->img->alt : '',

					// template
					'template' 	=> APP_VIEWS . '/_cards/card-2.html'

				));

			}

		}

		// homepage and contacts
		$home = ZetaData::get_pages('home');
		$contacts = ZetaData::get_pages('contacts');

		// setup template
		$results_html = get_html(array(

			// items
			'items'		=> $items_html,

			// conent
			'name'		=> $home->know_how_name,
			'title'		=> $home->know_how_title,
			'button'	=> $home->know_how_button,
			'link'		=> ZetaData::get_lang_url($contacts),

			// template
			'template' 	=> APP_VIEWS . '/home/home-know-how.html'

		));

		// nothing found
		return $results_html;

	}





	/* --------------------------------- *\
 
		Automations

	\* --------------------------------- */

	/**
	 * Template for Automations in the Austra page
	 * 
	 * @return html string
	*/
	public static function automations() {

		// variable
		$items_html = '';

		// get the children
		if($items = ZetaData::automations()) {

			// setup
			foreach($items as $key => $item) {

				// setup template
				$items_html .= get_html(array(

					// content
					'name'		=> $item->name,

					// image
					'image'		=> $item->img ? $item->img->src : false,
					'image_alt'	=> $item->img ? $item->img->alt : '',

					// template
					'template' 	=> APP_VIEWS . '/_cards/card-4.html'

				));

			}

		}

		// get tab content
		$tab = ZetaData::get_tabs('about');

		// setup template
		$results_html = get_html(array(

			// title
			'title'		=> $tab->info_3_title,
			'subtitle'	=> $tab->info_3_subtitle,

			// items
			'items'		=> $items_html,

			// template
			'template' 	=> APP_VIEWS . '/austra/austra-automations.html'

		));

		// nothing found
		return $results_html;

	}





	/* --------------------------------- *\
 
		Austra Tabs

	\* --------------------------------- */

	/**
	 * tabs
	 * 
	 * @param $url - array - current url
	 * @return html string
	*/
	public static function tabs($url) {

		// variable
		$tabs_html = '';
		$tabs_menu_html = '';

		// active modules
		$active_module = false;
		if(isset($url[3])) $active_module = $url[3];

		// get the children
		if($tabs = ZetaData::get_tabs()) {

			$tab_count = 1;

			foreach($tabs as $tab_id => $tab) {

				// active
				$active_button = '';
				$active_tab = '';

				// first active if url not set
				if($tab_count == 1 && !isset($url[2])) {
					$active_button = '  austra-tabs__menu__active';
					$active_tab = '  austra-tabs__active';
				}

				// check for tab in url
				if(isset($url[2]) && $url[2] == $tab->alias) {
					$active_button = '  austra-tabs__menu__active';
					$active_tab = '  austra-tabs__active';
				}

				$tabs_menu_html .= get_html(array(

					// content
					'name'		=> $tab->name,
					'link'		=> $tab->alias,
					'active'	=> $active_button,
					'class'		=> $tab_count > 1 ? '  margin-l' : '',
					'tab_id'	=> $tab_id,

					// template
					'template'	=> APP_VIEWS . '/austra/austra-tabs-button.html'

				));

				if($tab_id == 'about') {

					$tabs_html .= get_html(array(
						
						// current tab
						'active'	=> $active_tab,
						'tab_id'	=> $tab_id,

						// tab content
						'info_1'	=> self::block_2($tab->info_1, $tab->info_1_img),
						'info_2'	=> self::block_3_right($tab->info_2, $tab->info_2_img),
						'info_3'	=> self::automations(),
						'info_4'	=> self::block_4($tab->info_4, $tab->info_4_img),

						// template
						'template'	=> APP_VIEWS . '/austra/' . $tab->template

					));

				}

				if($tab_id == 'modules') {

					$tabs_html .= get_html(array(

						// current tab
						'active'		=> $active_tab,
						'tab_id'		=> $tab_id,

						// tab content
						'module_menu' 	=> self::module_menu($active_module),
						'modules' 		=> self::modules($active_module),
						'use_cases' 	=> self::use_cases_summary('austra'),

						// template
						'template'		=> APP_VIEWS . '/austra/' . $tab->template

					));

				}

				if($tab_id == 'presentations') {

					$tabs_html .= get_html(array(

						// current tab
						'active'		=> $active_tab,
						'tab_id'		=> $tab_id,

						// content
						'title'			=> $tab->title,
						'subtitle'		=> $tab->subtitle,
						'presentations' => self::presentations(false, 'austra'),

						// template
						'template'	=> APP_VIEWS . '/austra/' . $tab->template

					));

				}

				// count
				$tab_count++;

			}

		}

		// setup template
		$results_html = get_html(array(

			// items
			'tabs_menu'		=> $tabs_menu_html,
			'tabs_content'	=> $tabs_html,

			// template
			'template' 	=> APP_VIEWS . '/austra/austra-tabs.html'

		));

		// nothing found
		return $results_html;

	}





	/* --------------------------------- *\
 
		Use Cases

	\* --------------------------------- */

	/**
	 * Use Cases full page
	 * 
	 * @param $limit - int - how many cases to show
	 * @return html string
	*/
	public static function use_cases($limit = false) {

		// variable
		$items_html = '';

		// get the children
		if($items = ZetaData::use_cases($limit)) {

			foreach($items as $key => $item) {

				// set the number
				$number = $key + 1;

				// check even odd
				$template = 'odd';
				if($number % 2 == 0) {
					$template = 'even';
				}

				// check modules
				$item_modules = '';
				if($item->modules && is_array($item->modules)) {
					foreach($item->modules as $module) {
						$item_modules .= '<div class="hashtag"><span>' . $module . '</span></div>';
					}
				}

				// check integration
				$item_integration = '';
				if($item->integration && is_array($item->integration)) {
					foreach($item->integration as $integration) {
						$item_integration .= '<div>' . $integration . '</div>';
					}
				}

				// setup template
				$items_html .= get_html(array(

					// content
					'url'			=> $item->url,
					'name'			=> $item->name,
					'web'			=> $item->web ? $item->web : false,
					'web_text'		=> $item->web ? str_replace('https://', '', $item->web) : false,
					'products'		=> $item->products ? $item->products : false,
					'overview'		=> $item->overview ? $item->overview : false,
					'modules'		=> $item->modules ? $item_modules : false,
					'integration'	=> $item->integration ? $item_integration : false,

					// image
					'image'			=> $item->img ? $item->img->src : false,
					'image_alt'		=> $item->img ? $item->img->alt : '',
					'logo'			=> $item->logo ? $item->logo->src : false,
					'logo_alt'		=> $item->logo ? $item->logo->alt : '',

					// template
					'template' 		=> APP_VIEWS . '/use_cases/use-case-'.$template.'.html'

				));

			}

		}

		// nothing found
		return $items_html;

	}

	/**
	 * Use Cases summary block on Home and Austra page
	 * 
	 * @param $lang - string - current language
	 * @param $page - string - current page
	 * @return html string
	*/
	public static function use_cases_summary($page = false) {

		// variable
		$items_html = '';
		$lang = Config::read('lang_current');

		// pages
		$use_cases_page = ZetaData::get_pages('use-cases');
		$use_cases_url = ZetaData::get_lang_url($use_cases_page);
		
		// get the itmes
		$items_all = ZetaData::use_cases();
		$items = array($items_all[3], $items_all[1], $items_all[2]);

		// get the children
		if($items) {

			foreach($items as $key => $item) {

				$content = '
					<div class="text  title-small  margin-b-small">
						Company: <strong>' . $item->name . '</strong>
					</div>
					<div class="text  text-xsmall">
						' . $item->products . '
					</div>
				';

				// setup template
				$items_html .= get_html(array(

					// content
					'link'			=> $use_cases_url . '#' . $item->url,
					'content'		=> $content,

					// image
					'image'			=> $item->img ? $item->img->src : false,
					'image_alt'		=> $item->img ? $item->img->alt : '',

					// logo
					'logo'			=> $item->logo ? $item->logo->src : false,
					'logo_alt'		=> $item->logo ? $item->logo->alt : '',

					// template
					'template' 		=> APP_VIEWS . '/_cards/card-1.html'

				));

			}

		}

		// setup template
		$results_html = get_html(array(

			// items
			'items'		=> $items_html,
			'class'		=> $page && $page == 'austra' ? '  color-gray-bg' : '',

			// content
			'title'		=> $use_cases_page->name,
			'summary'	=> $use_cases_page->summary,
			'view_all'	=> $use_cases_page->view_all,
			'link'		=> $use_cases_url,

			// template
			'template' 	=> APP_VIEWS . '/use_cases/use-cases-summary.html'

		));

		// nothing found
		return $results_html;

	}





	/* --------------------------------- *\
 
		Team

	\* --------------------------------- */

	/**
	 * Team
	 * @return html string
	*/
	public static function team() {

		// variable
		$items_html = '';

		// get the children
		if($items = ZetaData::team()) {

			// setup
			foreach($items as $key => $item) {

				// setup template
				$items_html .= get_html(array(

					// content
					'name'		=> $item->name,
					'position'	=> $item->position,
					'linkedin'	=> $item->linkedin,

					// image
					'image'		=> $item->img ? $item->img->src : false,
					'image_alt'	=> $item->img ? $item->img->alt : '',

					// template
					'template' 	=> APP_VIEWS . '/_cards/card-3-team.html'

				));

			}

		}

		// nothing found
		return $items_html;

	}

	/**
	 * Block inviting to join the team
	 * 
	 * @return html string
	*/
	public static function team_join() {

		$team = ZetaData::get_pages('team');
		$contacts = ZetaData::get_pages('contacts');

		// template
		return get_html(array(

			'title' 		=> $team->join_title,
			'subtitle' 		=> $team->join_subtitle,

			'button_text'	=> $team->join_button,
			'button_link'	=> ZetaData::get_lang_url($contacts),

			// template
			'template'	=> APP_VIEWS . '/team/team-join.html'

		));

	}





	/* --------------------------------- *\
 
		Offices

	\* --------------------------------- */

	/**
	 * List of Offices used on the Home and Contacts page
	 * 
	 * @return html string
	*/
	public static function offices() {

		// variable
		$items_html = '';

		// get the children
		if($items = ZetaData::offices()) {

			// setup
			foreach($items as $key => $item) {

				// setup template
				$items_html .= get_html(array(

					// content
					'name'		=> $item->name,
					'company'	=> $item->company,
					'address'	=> $item->address,
					'phone'		=> $item->phone,
					'email'		=> $item->email,

					// template
					'template' 	=> APP_VIEWS . '/contacts/office.html'

				));

			}

		}

		// nothing found
		return $items_html;

	}

	/**
	 * Block with the offices in the footer section of Home
	 * 
	 * @return html string
	*/
	public static function offices_footer() {

		// contacts
		$contacts = ZetaData::get_pages('contacts');

		// setup template
		return get_html(array(

			// content
			'title' 	=> $contacts->title,
			'offices' 	=> self::offices(),

			// template
			'template' 	=> APP_VIEWS . '/contacts/offices-footer.html'

		));

	}





	/* --------------------------------- *\
 
		Blocks

	\* --------------------------------- */

	/**
	 * Home page intro block
	 * 
	 * @return html string
	*/
	public static function intro_home() {

		// get homepage and contacts
		$home = ZetaData::get_pages('home');
		$contacts = ZetaData::get_pages('contacts');

		// setup hashtags
		$hashtags = '';
		if(isset($home->hashtags) and is_array($home->hashtags)) {
			foreach($home->hashtags as $hashtag) {
				$hashtags .= '<div class="hashtag"><span>'.$hashtag.'</span></div>';
			}
		}

		// template
		return get_html(array(

			// content
			'title'		=> $home->title,
			'hashtags'	=> $hashtags,

			// contacts cta
			'cta_title'	=> $home->cta_title,
			'cta_text'	=> $home->cta_text,
			'cta_link'	=> ZetaData::get_lang_url($contacts),

			// template
			'template'	=> APP_VIEWS . '/home/home-intro.html'

		));

	}

	/**
	 * A simple intro block with title and subtitle
	 * on a dark blue background
	 * 
	 * @return html string
	*/
	public static function intro_simple($page) {

 		// check what page we om
		$page = ZetaData::get_pages($page);

		// template
		return get_html(array(

			'title' => $page->title,
			'text'	=> $page->subtitle,

			// template
			'template'	=> APP_VIEWS . '/_blocks/intro-simple.html'

		));

	}

	/**
	 * Block 1
	 * 
	 * @return html string
	*/
	public static function block_1($content, $img = false) {

		// template
		return get_html(array(

			// content
			'content'	=> $content,

			// image
			'image'		=> $img ? $img->src : false,
			'image_alt'	=> $img ? $img->alt : '',

			// template
			'template'	=> APP_VIEWS . '/_blocks/block-1.html'

		));

	}

	/**
	 * Block 2
	 * 
	 * @return html string
	*/
	public static function block_2($content, $img = false) {

		// template
		return get_html(array(

			// content
			'content'	=> $content,

			// image
			'image'		=> $img ? $img->src : false,
			'image_alt'	=> $img ? $img->alt : '',

			// template
			'template'	=> APP_VIEWS . '/_blocks/block-2.html'

		));

	}

	/**
	 * Block 3 - image on the right
	 * 
	 * @return html string
	*/
	public static function block_3_right($content, $img = false) {

		// template
		return get_html(array(

			// content
			'content'	=> $content,

			// image
			'image'		=> $img ? $img->src : false,
			'image_alt'	=> $img ? $img->alt : '',

			// template
			'template'	=> APP_VIEWS . '/_blocks/block-3-right.html'

		));

	}

	/**
	 * Block 3 - image on the left
	 * 
	 * @return html string
	*/
	public static function block_3_left($content, $img = false) {

		// template
		return get_html(array(

			// content
			'content'	=> $content,

			// image
			'image'		=> $img ? $img->src : false,
			'image_alt'	=> $img ? $img->alt : '',

			// template
			'template'	=> APP_VIEWS . '/_blocks/block-3-left.html'

		));

	}

	/**
	 * Block 4
	 * 
	 * @return html string
	*/
	public static function block_4($content, $img = false) {

		// template
		return get_html(array(

			// content
			'content'	=> $content,

			// image
			'image'		=> $img ? $img->src : false,
			'image_alt'	=> $img ? $img->alt : '',

			// template
			'template'	=> APP_VIEWS . '/_blocks/block-4.html'

		));

	}

	/**
	 * Block with text on left and button on right
	 * with a blue background
	 * 
	 * @return html string
	*/
	public static function block_contacts() {

 		// contacts
		$contacts = ZetaData::get_pages('contacts');

		// template
		return get_html(array(

			'title' 		=> $contacts->block_title,

			'button_text'	=> $contacts->block_button,
			'button_link'	=> ZetaData::get_lang_url($contacts),

			// template
			'template'	=> APP_VIEWS . '/_blocks/block-with-button.html'

		));

	}

}





// -- class.ZetaTemplates.php