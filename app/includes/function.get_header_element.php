<?php

/**
 * Get heaader stuff
 * @param string $what - what do we want to load?
 * @return string - output for the header
*/

function get_header_element($what, $global_files = false) {

	// default
	$result = '';

	// logic for css and js
	if($what == 'css' || $what == 'js') {

		switch ($what) {

			case 'css':
				$pattern = APP_SERVER == 'dev' ? '*-*.css' : '*.min.css';
				$output  = APP_SERVER != 'dev' ? '<link href="{{file}}" rel="preload" as="style" />' : '';
				$output  .= '<link type="text/css" href="{{file}}" rel="stylesheet" />';
				$folder  = APP_CSS;
				break;

			case 'js':
				$pattern = APP_SERVER == 'dev' ? '*-*.js' : '*.min.js';
				$output  = '<script src="{{file}}" type="text/javascript" defer></script>';
				$folder  = APP_JS;
				break;

		}

		if(is_dir(APP_PATH . $folder)) {

			// update title
			$result .= "<!-- $what -->\n";

			foreach(glob(APP_PATH . $folder . '/' . $pattern) as $file) {
				
				// remove path and add time query string
				$file = str_replace(APP_PATH . '/', '', $file);
				$file .= '?v=' . filemtime($file);

				// set the result
				$result .= "\t" . ( str_replace("{{file}}", $file, $output) ) . "\n";

			}

		} else {

			// no folder
			error_log("directory $folder is missing");

		}

		// load global files only on dev
		if(APP_SERVER == 'dev') {

			// load global files
			if($global_files && is_array($global_files)) {

				foreach ($global_files as $file) {

					// create the src
					$file = "assets/$what/$file";

					// set the result
					$result .= "\t" . ( str_replace("{{file}}", $file, $output) ) . "\n";

				}

			}

		}

	}

	// empty string
	return $result;

}





// -- function.get_header_element.php --