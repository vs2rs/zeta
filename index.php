<?php

/* --------------------------------- *\
 
	Project: Zeta

\* --------------------------------- */

// load our app
require_once('app/setup.php');
require_once('app/includes/function.split_url.php');
require_once('app/includes/function.get_html.php');
require_once('app/includes/function.get_header_element.php');
require_once('app/includes/function.validate_string.php');
require_once('app/includes/function.send_mail.php');
require_once('app/includes/function.get_meta.php');
require_once('app/includes/class.Config.php');
require_once('app/includes/class.ZetaData.php');
require_once('app/includes/class.ZetaTemplates.php');
require_once('app/includes/class.Form.php');
require_once('app/includes/class.ZetaForm.php');

/* --------------------------------- *\
 
	Load page

\* --------------------------------- */

// get the url
$url = split_url(strip_tags($_SERVER['REQUEST_URI']), APP_FOLDER);

// set the defaults
$lang = Config::read('lang_default');
$page = 'error';
$page_id = 'home';

// check for language
if(isset($url[0])) {

	// check for language
	if(in_array($url[0], Config::read('languages'))) {

		// add to languages
		Config::write('lang_current', $url[0]);

		// check if page set if not load home
		if(!isset($url[1])) {
			$page = Config::read('homepage');
		}

	}

} else {

	// load home if nothing set in the url
	$page = Config::read('homepage');

}

// get pages
$pages = ZetaData::get_pages();

// check for a specific page
if(isset($url[0]) && validate_string($url[0], 'alias') && isset($pages[$url[0]])) {
	array_unshift($url, $lang);
	$page = $pages[$url[1]]->template;
	$page_id = $url[1];
}

// check for a specific page
if(isset($url[1]) && validate_string($url[1], 'alias') && isset($pages[$url[1]])) {
	$page = $pages[$url[1]]->template;
	$page_id = $url[1];
}

// check for extra header on page
if(file_exists("app/views/$page/___head.php")) {
	require_once("app/views/$page/___head.php");
}

// include the head and header
require_once("app/views/_global/_head.php");
require_once("app/views/_global/_header.php");

// include the page
require_once("app/views/$page/__$page.php");

// footer stuff
require_once("app/views/_global/_footer.php");
require_once("app/views/_global/_end.php");





// -- index.php