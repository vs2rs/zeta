<?php

/**
 * Create value for <title> tag
 * @param page = object of current page
 */

function get_meta_title($page) {

	if(isset($page->meta)
	&& isset($page->meta->title)
	&& $page->meta->title != '') {
		return $page->meta->title;
	}

	// get site name
	$site_name = Config::read('site_name');

	// retunr page title and site name combo
	return $page->name . ( $site_name ? ' | ' . $site_name : '' );

}

/**
 * Create meta tags html
 * @param page - object - current page we on
 */

function get_meta($page) {

	/**

	 * Basic meta

	*/

	/**

	 * Check for author

	*/

	if(isset($page->meta) && !isset($page->meta->author)) {
		if(Config::read('meta_author')) {
			$page->meta->author = Config::read('meta_author');
		}
	}

	$meta_output = '';

	if(is_object($page->meta) && count((array)$page->meta) > 0) {

		$meta_output .= "<!-- basic meta tags -->" . "\n";

		foreach ($page->meta as $key => $value) {
			if($value != "" && $key != "title") {
				$meta_output .= "\t" . '<meta name="'.$key.'" content="'.htmlspecialchars($value, ENT_QUOTES, 'UTF-8').'" />' . "\n";
			}
		}

	}

	/**

	 * Facebook

	*/

	/**

	 * Check for siet name

	*/

	if(isset($page->facebook) && !isset($page->facebook->site_name)) {
		if(Config::read('site_name')) {
			$page->facebook->site_name = Config::read('site_name');
		}
	}

	$meta_output .= "\n";

	if(is_object($page->facebook) && count((array)$page->facebook) > 0) {

		$meta_output .= "\t" . "<!-- facebook meta tags -->" . "\n";

		foreach ($page->facebook as $key => $value) {
			// set width and height keys
			if($key == "image_width") $key = "image:width";
			if($key == "image_height") $key = "image:height";
			// check value - except description - we nedd to output even if empty
			if($value != "" || $key == "description") {
				// output
				$meta_output .=
				"\t" . '<meta property="og:'.$key.'" content="'
				. htmlspecialchars($value, ENT_QUOTES, 'UTF-8')
				. '" />' . "\n";
			}

		}

	}

	return $meta_output;

}





// -- __function.get_meta.php