<?php

/* --------------------------------- *\
 
	Home

\* --------------------------------- */

// get pages and data
$page_current = ZetaData::get_pages('home');
$austra = ZetaData::get_pages('austra');
$presentations = ZetaData::get_tabs('presentations');

// template
echo get_html(array(

	// data
	'intro'			=> ZetaTemplates::intro_home(),
	'austra'		=> ZetaTemplates::block_1($page_current->austra, $page_current->austra_img),
	'presentations' => ZetaTemplates::presentations(2),
	'offices' 		=> ZetaTemplates::offices_footer(),
	'use_cases' 	=> ZetaTemplates::use_cases_summary($lang),
	'know_how' 		=> ZetaTemplates::know_how(),

	// presentation titles
	'p_title'		=> $presentations->title,
	'p_subtitle'	=> $presentations->subtitle,
	'p_button'		=> $presentations->button,
	'p_link'		=> ZetaData::get_lang_url($austra) . '/' . $presentations->alias,

	// template
	'template'		=> APP_VIEWS . '/home/home.html'

));





// -- __home.php