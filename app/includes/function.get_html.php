<?php

/**
 * Get HTML file
 * @param array $params list to replace in template
 * @return string - html file or empty string if no file
*/
function get_html($params) {

	// check if template set
	if(isset($params['template'])) {
		$template = $params['template'];
		unset($params['template']);
	} else {
		php_error_log("template not set");
	}

	// is the template set?
	if(isset($template) && file_exists($template)) {

		// get the template
		$template = file_get_contents($template);
		$template = process_template_vars($template, $params);

		// minify our html output
		if(MINIFY_HTML) {

			$search = array(
				'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
				'/[^\S ]+\</s',     // strip whitespaces before tags, except space
				'/(\s)+/s',         // shorten multiple whitespace sequences
				'/<!--(.|\s)*?-->/' // Remove HTML comments
			);

			$replace = array('>','<','\\1','');

			$template = preg_replace($search, $replace, $template);
			$template = str_replace('" />', '"/>', $template);

		}

		// results
		return $template;

	}

	// remove some of the path
	$template = str_replace(APP_VIEWS, '', $template);

	// check for file the function was called from
	$caller = array_search(__FUNCTION__, array_column(debug_backtrace(), 'function'));
	$file = str_replace(APP_VIEWS, '', debug_backtrace()[$caller]['file']);
	$line = str_replace(APP_VIEWS, '', debug_backtrace()[$caller]['line']);

	// no file found
	error_log("get_html() template $template not found in $file on line $line");

	// return empty
	return '';

}

/**
 * Process {{params}}
 * @param string $template - our html template
 * @param params - array of params
 * @return html string
*/
function process_template_vars($template, $params) {

	// get everyting between {{tag}} ... {{/tag}}
	preg_match_all("/{{(.*?)}}(.*?){{\/\\1}}/s", $template, $result);

	// run the clean up
	foreach ($result[0] as $key => $tag) {

		// set tag name and content
		$tag_name = $result[1][$key];
		$tag_content = $result[2][$key];

		// check if tag name is set and is true
		if(isset($params[$tag_name]) && $params[$tag_name] !== false) {
			$template = str_replace($tag, $tag_content, $template);
		} else {
			$template = str_replace($tag, '', $template);
		}

	}

	// colloect the remaining tags
	preg_match_all("/{{(.*?)}}/", $template, $results);
	$tags = array_unique($results[0]);
	$tag_names = array_unique($results[1]);

	// replace the tags
	foreach ($tags as $key => $tag) {

		if(is_array($tag)) { return ''; }

		if(is_array($template)) { return ''; }

		if(isset($params[$tag_names[$key]])
		&& is_array($params[$tag_names[$key]])) {
			return $tag_names[$key];
		}

		if(isset($params[$tag_names[$key]])) {
			$template = str_replace(
				$tag, $params[$tag_names[$key]], $template
			);
		}
	}

	return $template;

}





// -- function.get_html.php --